--- drivers/block/ps3disk.c	2013-02-14 11:19:47.200033102 +0100
+++ drivers/block/ps3disk.c	2013-02-14 11:22:05.716017676 +0100
@@ -445,85 +433,67 @@
 		goto fail_free_priv;
 	}
 
-	for (regidx = 0; regidx < dev->num_regions; regidx++)
-		dev->regions[regidx].flags = region_flags[regidx];
-
 	error = ps3stor_setup(dev, ps3disk_interrupt);
 	if (error)
 		goto fail_free_bounce;
 
 	ps3disk_identify(dev);
 
-	for (devidx = 0; devidx < dev->num_regions; devidx++)
-	{
-		if (test_bit(devidx, &dev->accessible_regions) == 0)
-			continue;
+	queue = blk_init_queue(ps3disk_request, &priv->lock);
+	if (!queue) {
+		dev_err(&dev->sbd.core, "%s:%u: blk_init_queue failed\n",
+			__func__, __LINE__);
+		error = -ENOMEM;
+		goto fail_teardown;
+	}
 
-		queue = blk_init_queue(ps3disk_request, &priv->lock);
-		if (!queue) {
-			dev_err(&dev->sbd.core, "%s:%u: blk_init_queue failed\n",
-				__func__, __LINE__);
-			error = -ENOMEM;
-			goto fail_cleanup;
-		}
+	priv->queue = queue;
+	queue->queuedata = dev;
 
-		priv->queue[devidx] = queue;
-		queue->queuedata = dev;
+	blk_queue_bounce_limit(queue, BLK_BOUNCE_HIGH);
 
-		blk_queue_bounce_limit(queue, BLK_BOUNCE_HIGH);
+	blk_queue_max_hw_sectors(queue, dev->bounce_size >> 9);
+	blk_queue_segment_boundary(queue, -1UL);
+	blk_queue_dma_alignment(queue, dev->blk_size-1);
+	blk_queue_logical_block_size(queue, dev->blk_size);
 
-		blk_queue_max_hw_sectors(queue, dev->bounce_size >> 9);
-		blk_queue_segment_boundary(queue, -1UL);
-		blk_queue_dma_alignment(queue, dev->blk_size-1);
-		blk_queue_logical_block_size(queue, dev->blk_size);
-
-		blk_queue_flush(queue, REQ_FLUSH);
-
-		blk_queue_max_segments(queue, -1);
-		blk_queue_max_segment_size(queue, dev->bounce_size);
-
-		gendisk = alloc_disk(PS3DISK_MINORS);
-		if (!gendisk) {
-			dev_err(&dev->sbd.core, "%s:%u: alloc_disk failed\n", __func__,
-				__LINE__);
-			error = -ENOMEM;
-			goto fail_cleanup;
-		}
+	blk_queue_flush(queue, REQ_FLUSH);
 
-		priv->gendisk[devidx] = gendisk;
-		gendisk->major = ps3disk_major;
-		gendisk->first_minor = devidx * PS3DISK_MINORS;
-		gendisk->fops = &ps3disk_fops;
-		gendisk->queue = queue;
-		gendisk->private_data = dev;
-		gendisk->driverfs_dev = &dev->sbd.core;
-		snprintf(gendisk->disk_name, sizeof(gendisk->disk_name), PS3DISK_NAME,
-			 devidx+'a');
-		priv->blocking_factor = dev->blk_size >> 9;
-		set_capacity(gendisk,
-		   	 dev->regions[devidx].size*priv->blocking_factor);
-
-		dev_info(&dev->sbd.core,
-			 "%s is a %s (%llu MiB total, %lu MiB region)\n",
-			 gendisk->disk_name, priv->model, priv->raw_capacity >> 11,
-			 get_capacity(gendisk) >> 11);
+	blk_queue_max_segments(queue, -1);
+	blk_queue_max_segment_size(queue, dev->bounce_size);
 
-		add_disk(gendisk);
+	gendisk = alloc_disk(PS3DISK_MINORS);
+	if (!gendisk) {
+		dev_err(&dev->sbd.core, "%s:%u: alloc_disk failed\n", __func__,
+			__LINE__);
+		error = -ENOMEM;
+		goto fail_cleanup_queue;
 	}
 
+	priv->gendisk = gendisk;
+	gendisk->major = ps3disk_major;
+	gendisk->first_minor = devidx * PS3DISK_MINORS;
+	gendisk->fops = &ps3disk_fops;
+	gendisk->queue = queue;
+	gendisk->private_data = dev;
+	gendisk->driverfs_dev = &dev->sbd.core;
+	snprintf(gendisk->disk_name, sizeof(gendisk->disk_name), PS3DISK_NAME,
+		 devidx+'a');
+	priv->blocking_factor = dev->blk_size >> 9;
+	set_capacity(gendisk,
+		     dev->regions[dev->region_idx].size*priv->blocking_factor);
+
+	dev_info(&dev->sbd.core,
+		 "%s is a %s (%llu MiB total, %lu MiB for OtherOS)\n",
+		 gendisk->disk_name, priv->model, priv->raw_capacity >> 11,
+		 get_capacity(gendisk) >> 11);
+
+	add_disk(gendisk);
 	return 0;
 
-fail_cleanup:
-	for (devidx = 0; devidx < dev->num_regions; devidx++)
-	{
-		if (priv->gendisk[devidx]) {
-			del_gendisk(priv->gendisk[devidx]);
-			put_disk(priv->gendisk[devidx]);
-		}
-
-		if (priv->queue[devidx])
-			blk_cleanup_queue(priv->queue[devidx]);
-	}
+fail_cleanup_queue:
+	blk_cleanup_queue(queue);
+fail_teardown:
 	ps3stor_teardown(dev);
 fail_free_bounce:
 	kfree(dev->bounce_buf);
