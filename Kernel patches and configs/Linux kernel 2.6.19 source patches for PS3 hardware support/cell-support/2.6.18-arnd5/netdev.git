commit 9b6b0b81f0de6a17ce57c818d1f403253c200e1f
Author: Jim Lewis <jklewis@us.ibm.com>
Date:   Fri Sep 22 17:22:53 2006 -0700

    [PATCH] Spidernet: add ethtool -S (show statistics)
    
    Add the ethtool -S (show statistics) feature to the Spidernet ethernet
    driver.  I have tested it extensively and believe it is ready to be
    applied.
    
    Signed-off-by: James K Lewis <jklewis@us.ibm.com>
    Signed-off-by: Andrew Morton <akpm@osdl.org>
    Signed-off-by: Jeff Garzik <jeff@garzik.org>

commit 9b6b0b81f0de6a17ce57c818d1f403253c200e1f
Author: Jim Lewis <jklewis@us.ibm.com>
Date:   Fri Sep 22 17:22:53 2006 -0700

    [PATCH] Spidernet: add ethtool -S (show statistics)
    
    Add the ethtool -S (show statistics) feature to the Spidernet ethernet
    driver.  I have tested it extensively and believe it is ready to be
    applied.
    
    Signed-off-by: James K Lewis <jklewis@us.ibm.com>
    Signed-off-by: Andrew Morton <akpm@osdl.org>
    Signed-off-by: Jeff Garzik <jeff@garzik.org>

Index: linux-2.6/drivers/net/spider_net.c
===================================================================
--- linux-2.6.orig/drivers/net/spider_net.c
+++ linux-2.6/drivers/net/spider_net.c
@@ -403,6 +403,7 @@ spider_net_prepare_rx_descr(struct spide
 	if (!descr->skb) {
 		if (netif_msg_rx_err(card) && net_ratelimit())
 			pr_err("Not enough memory to allocate rx buffer\n");
+		card->spider_stats.alloc_rx_skb_error++;
 		return -ENOMEM;
 	}
 	descr->buf_size = bufsize;
@@ -423,6 +424,7 @@ spider_net_prepare_rx_descr(struct spide
 		dev_kfree_skb_any(descr->skb);
 		if (netif_msg_rx_err(card) && net_ratelimit())
 			pr_err("Could not iommu-map rx buffer\n");
+		card->spider_stats.rx_iommu_map_error++;
 		descr->dmac_cmd_status = SPIDER_NET_DESCR_NOT_IN_USE;
 	} else {
 		descr->dmac_cmd_status = SPIDER_NET_DESCR_CARDOWNED |
@@ -651,6 +653,7 @@ spider_net_prepare_tx_descr(struct spide
 		if (netif_msg_tx_err(card) && net_ratelimit())
 			pr_err("could not iommu-map packet (%p, %i). "
 				  "Dropping packet\n", skb->data, skb->len);
+		card->spider_stats.tx_iommu_map_error++;
 		return -ENOMEM;
 	}
 
@@ -818,6 +821,7 @@ spider_net_xmit(struct sk_buff *skb, str
 	}
 
 	if (spider_net_get_descr_status(descr) != SPIDER_NET_DESCR_NOT_IN_USE) {
+		card->netdev_stats.tx_dropped++;
 		result = NETDEV_TX_LOCKED;
 		goto out;
 	}
@@ -913,6 +917,7 @@ spider_net_pass_skb_up(struct spider_net
 			pr_err("error in received descriptor found, "
 			       "data_status=x%08x, data_error=x%08x\n",
 			       data_status, data_error);
+		card->spider_stats.rx_desc_error++;
 		return 0;
 	}
 
@@ -1010,9 +1015,11 @@ spider_net_decode_one_descr(struct spide
 
 	if ( (status != SPIDER_NET_DESCR_COMPLETE) &&
 	     (status != SPIDER_NET_DESCR_FRAME_END) ) {
-		if (netif_msg_rx_err(card))
+		if (netif_msg_rx_err(card)) {
 			pr_err("%s: RX descriptor with state %d\n",
 			       card->netdev->name, status);
+			card->spider_stats.rx_desc_unk_state++;
+		}
 		goto refill;
 	}
 
@@ -1934,6 +1941,7 @@ spider_net_tx_timeout(struct net_device 
 		schedule_work(&card->tx_timeout_task);
 	else
 		atomic_dec(&card->tx_timeout_task_counter);
+	card->spider_stats.tx_timeouts++;
 }
 
 /**
Index: linux-2.6/drivers/net/spider_net.h
===================================================================
--- linux-2.6.orig/drivers/net/spider_net.h
+++ linux-2.6/drivers/net/spider_net.h
@@ -415,6 +415,15 @@ struct spider_net_options {
 					  NETIF_MSG_HW | \
 					  NETIF_MSG_WOL )
 
+struct spider_net_extra_stats {
+	unsigned long rx_desc_error;
+	unsigned long tx_timeouts;
+	unsigned long alloc_rx_skb_error;
+	unsigned long rx_iommu_map_error;
+	unsigned long tx_iommu_map_error;
+	unsigned long rx_desc_unk_state;
+};
+
 struct spider_net_card {
 	struct net_device *netdev;
 	struct pci_dev *pdev;
@@ -439,9 +448,9 @@ struct spider_net_card {
 
 	/* for ethtool */
 	int msg_enable;
-
 	int rx_desc;
 	int tx_desc;
+	struct spider_net_extra_stats spider_stats;
 
 	struct spider_net_descr descr[0];
 };
Index: linux-2.6/drivers/net/spider_net_ethtool.c
===================================================================
--- linux-2.6.orig/drivers/net/spider_net_ethtool.c
+++ linux-2.6/drivers/net/spider_net_ethtool.c
@@ -27,6 +27,27 @@
 
 #include "spider_net.h"
 
+
+#define SPIDER_NET_NUM_STATS 13
+
+static struct {
+	const char str[ETH_GSTRING_LEN];
+} ethtool_stats_keys[] = {
+	{ "tx_packets" },
+	{ "tx_bytes" },
+	{ "rx_packets" },
+	{ "rx_bytes" },
+	{ "tx_errors" },
+	{ "tx_dropped" },
+	{ "rx_dropped" },
+	{ "rx_descriptor_error" },
+	{ "tx_timeouts" },
+	{ "alloc_rx_skb_error" },
+	{ "rx_iommu_map_error" },
+	{ "tx_iommu_map_error" },
+	{ "rx_desc_unk_state" },
+};
+
 static int
 spider_net_ethtool_get_settings(struct net_device *netdev,
 			       struct ethtool_cmd *cmd)
@@ -142,6 +163,37 @@ spider_net_ethtool_get_ringparam(struct 
 	ering->rx_pending = card->rx_desc;
 }
 
+static int spider_net_get_stats_count(struct net_device *netdev)
+{
+	return SPIDER_NET_NUM_STATS;
+}
+
+static void spider_net_get_ethtool_stats(struct net_device *netdev,
+		struct ethtool_stats *stats, u64 *data)
+{
+	struct spider_net_card *card = netdev->priv;
+
+	data[0] = card->netdev_stats.tx_packets;
+	data[1] = card->netdev_stats.tx_bytes;
+	data[2] = card->netdev_stats.rx_packets;
+	data[3] = card->netdev_stats.rx_bytes;
+	data[4] = card->netdev_stats.tx_errors;
+	data[5] = card->netdev_stats.tx_dropped;
+	data[6] = card->netdev_stats.rx_dropped;
+	data[7] = card->spider_stats.rx_desc_error;
+	data[8] = card->spider_stats.tx_timeouts;
+	data[9] = card->spider_stats.alloc_rx_skb_error;
+	data[10] = card->spider_stats.rx_iommu_map_error;
+	data[11] = card->spider_stats.tx_iommu_map_error;
+	data[12] = card->spider_stats.rx_desc_unk_state;
+}
+
+static void spider_net_get_strings(struct net_device *netdev, u32 stringset,
+				   u8 *data)
+{
+	memcpy(data, ethtool_stats_keys, sizeof(ethtool_stats_keys));
+}
+
 struct ethtool_ops spider_net_ethtool_ops = {
 	.get_settings		= spider_net_ethtool_get_settings,
 	.get_drvinfo		= spider_net_ethtool_get_drvinfo,
@@ -154,5 +206,8 @@ struct ethtool_ops spider_net_ethtool_op
 	.get_tx_csum		= spider_net_ethtool_get_tx_csum,
 	.set_tx_csum		= spider_net_ethtool_set_tx_csum,
 	.get_ringparam          = spider_net_ethtool_get_ringparam,
+	.get_strings		= spider_net_get_strings,
+	.get_stats_count	= spider_net_get_stats_count,
+	.get_ethtool_stats	= spider_net_get_ethtool_stats,
 };
 
