commit ba00003aa83a61b615542dd66f5af8fb4a7cee1d
Merge: 9c8e7f5... 40a5f7c...
Author: Linus Torvalds <torvalds@g5.osdl.org>
Date:   Sat Oct 7 10:52:00 2006 -0700

    Merge branch 'merge' of git://git.kernel.org/pub/scm/linux/kernel/git/paulus/powerpc
    
    * 'merge' of git://git.kernel.org/pub/scm/linux/kernel/git/paulus/powerpc:
      [POWERPC] Update iseries_defconfig
      [POWERPC] Fix fsl_soc build breaks
      [POWERPC] Minor fix for bootargs property
      [POWERPC] Update MTFSF_L() comment
      [POWERPC] Update pSeries defconfig for SATA
      [POWERPC] Don't get PCI IRQ from OF for devices with no IRQ
      [POWERPC] Fix zImage decompress location
      [POWERPC] linux,tce-size property is 32 bits
      [POWERPC] Add DTS for MPC8349E-mITX board
      [POWERPC] Fix harmless typo
      [PPC] Fix some irq breakage with ARCH=ppc

commit b75f3f751c17e01544bd6706e54efae35d66d446
Author: Stephen Rothwell <sfr@canb.auug.org.au>
Date:   Fri Oct 6 13:53:12 2006 +1000

    [POWERPC] Update iseries_defconfig
    
    This make sure that an iseries_defconfig does not inlude
    other platforms.
    
    Signed-off-by: Stephen Rothwell <sfr@canb.auug.org.au>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 2b00b254debd707571f20149dbd4b45264a120e5
Author: Olof Johansson <olof@lixom.net>
Date:   Thu Oct 5 21:16:48 2006 -0500

    [POWERPC] Fix fsl_soc build breaks
    
    Hrm, there's no way this ever built at time of merge. There's a missing } and
    the wrong type on phy_irq.
    
    Also, another const for get_property().
    
      CC      arch/powerpc/sysdev/fsl_soc.o
    arch/powerpc/sysdev/fsl_soc.c: In function 'fs_enet_of_init':
    arch/powerpc/sysdev/fsl_soc.c:625: error: assignment of read-only variable 'phy_irq'
    arch/powerpc/sysdev/fsl_soc.c:625: warning: assignment makes integer from pointer without a cast
    arch/powerpc/sysdev/fsl_soc.c:661: warning: assignment discards qualifiers from pointer target type
    arch/powerpc/sysdev/fsl_soc.c:684: error: subscripted value is neither array nor pointer
    arch/powerpc/sysdev/fsl_soc.c:687: error: subscripted value is neither array nor pointer
    arch/powerpc/sysdev/fsl_soc.c:722: warning: ISO C90 forbids mixed declarations and code
    arch/powerpc/sysdev/fsl_soc.c:728: error: invalid storage class for function 'cpm_uart_of_init'
    arch/powerpc/sysdev/fsl_soc.c:798: error: initializer element is not constant
    arch/powerpc/sysdev/fsl_soc.c:798: error: expected declaration or statement at end of input
    make[1]: *** [arch/powerpc/sysdev/fsl_soc.o] Error 1
    
    Signed-off-by: Olof Johansson <olof@lixom.net>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit c1ce464d29e5bbf5819b2d7527b7d3030a6d65f1
Author: Geoff Levand <geoffrey.levand@am.sony.com>
Date:   Thu Oct 5 11:35:16 2006 -0700

    [POWERPC] Minor fix for bootargs property
    
    Avoid the use of an uninitialized stack variable when the powerpc device tree
    bootargs property is either missing or incorrectly defined.  This also makes
    CONFIG_CMDLINE work properly under these conditions.  This change adds a test
    for the existence of the bootargs property.
    
    early_init_dt_scan_chosen() tests for a zero length bootargs property in its
    CONFIG_CMDLINE processing, but the current implementation of
    of_get_flat_dt_prop() doesn't assign a value to the length when no property is
    found.  Since an automatic variable is used, a stale value from the stack will
    be used in the test.
    
    Signed-off-by: Geoff Levand <geoffrey.levand@am.sony.com>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 52aed7cd52ce8a1d576e26976c3950512e1af8b6
Author: Anton Blanchard <anton@samba.org>
Date:   Fri Oct 6 02:54:07 2006 +1000

    [POWERPC] Update MTFSF_L() comment
    
    David Woodhouse points out that the comment accompanying the MTFSF_L
    macro is misleading. We should make it clear that the L bit is ignored
    on older CPUS, not the entire instruction.
    
    Signed-off-by: Anton Blanchard <anton@samba.org>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 3afbf5d6ef791be61d18329ae5302bbf1cd83723
Author: Brian King <brking@us.ibm.com>
Date:   Thu Oct 5 10:52:04 2006 -0500

    [POWERPC] Update pSeries defconfig for SATA
    
    Since the ipr driver now supports SATA and depends on libata,
    enable libata to get built.
    
    Signed-off-by: Brian King <brking@us.ibm.com>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 41550c5128150175197257b6ceab2cd50dea7b51
Author: Benjamin Herrenschmidt <benh@kernel.crashing.org>
Date:   Thu Oct 5 16:40:41 2006 +1000

    [POWERPC] Don't get PCI IRQ from OF for devices with no IRQ
    
    This patch adds checking of the PCI_INTERRUPT_PIN register before
    using standard OF parsing to retreive PCI interrupts. The reason is
    that some PCI devices may have no PCI interrupt, though they may have
    interrupts attached via other means. In this case, we shall not use
    irq->pdev, but device-specific code can later retreive those interrupts
    instead.
    
    Without that patch, Maple and derivatives don't get the right interrupt
    for the second IDE channel as the linux IDE code fallsback to the PCI
    irq instead of trying to use the legacy ones for the on-board controller
    (which has no PCI_INTERRUPT_PIN). Having no PCI IRQ assign to it (as it
    doesn't request any) fixes it.
    
    Signed-off-by: Benjamin Herrenschmidt <benh@kernel.crashing.org>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit c998de146061db17787c1a31a3db1989f1341fdf
Author: Benjamin Herrenschmidt <benh@kernel.crashing.org>
Date:   Thu Oct 5 14:18:46 2006 +1000

    [POWERPC] Fix zImage decompress location
    
    The zImage wrapper has a "hack" that force the decompression to happen
    above 20Mb for 64 bits kernels, to work around issues with some
    firmwares on the field. However, the new wrapper has a bug which makes
    that hack not work properly. This fixes it.
    
    Signed-off-by: Benjamin Herrenschmidt <benh@kernel.crashing.org>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 9938c474f39e416091db9993954996266a3f2edf
Author: Nathan Lynch <ntl@pobox.com>
Date:   Wed Oct 4 22:28:00 2006 -0500

    [POWERPC] linux,tce-size property is 32 bits
    
    The "linux,tce-size" property is only 32 bits (see
    prom_initialize_tce_table() in arch/powerpc/kernel/prom_init.c).
    Treating it as an unsigned long in iommu_table_setparms() leads to
    access beyond the end of the property's buffer, so we pass garbage to
    the memset() in that function.
    
    [boot]0020 XICS Init
    i8259 legacy interrupt controller initialized
    [boot]0021 XICS Done
    PID hash table entries: 4096 (order: 12, 32768 bytes)
    cpu 0x0: Vector: 300 (Data Access) at [c0000000fe783850]
        pc: c000000000035e90: .memset+0x60/0xfc
        lr: c000000000044fa4: .iommu_table_setparms+0xb0/0x158
        sp: c0000000fe783ad0
       msr: 9000000000009032
       dar: c000000100000000
     dsisr: 42010000
      current = 0xc00000000450e810
      paca    = 0xc000000000411580
        pid   = 1, comm = swapper
    enter ? for help
    [link register   ] c000000000044fa4 .iommu_table_setparms+0xb0/0x158
    [c0000000fe783ad0] c000000000044f4c .iommu_table_setparms+0x58/0x158
    (unreliable)
    [c0000000fe783b70] c00000000004529c
    .iommu_bus_setup_pSeries+0x1c4/0x254
    [c0000000fe783c00] c00000000002b8ac .do_bus_setup+0x3c/0xe4
    [c0000000fe783c80] c00000000002c924 .pcibios_fixup_bus+0x64/0xd8
    [c0000000fe783d00] c0000000001a2d5c .pci_scan_child_bus+0x6c/0x10c
    [c0000000fe783da0] c00000000002be28 .scan_phb+0x17c/0x1b4
    [c0000000fe783e40] c0000000003cfa00 .pcibios_init+0x58/0x19c
    [c0000000fe783ec0] c0000000000094b4 .init+0x1e8/0x3d8
    [c0000000fe783f90] c000000000026e54 .kernel_thread+0x4c/0x68
    
    Signed-off-by: Nathan Lynch <ntl@pobox.com>
    Acked-by: Olof Johansson <olof@lixom.net>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 74c37e8c9f626ed000388bebdc781a8e35ee4ab7
Author: Timur Tabi <timur@freescale.com>
Date:   Wed Oct 4 11:03:44 2006 -0500

    [POWERPC] Add DTS for MPC8349E-mITX board
    
    Add the DTS for the Freescale MPC 8349E-mITX reference board.  Contact
    Vitesse for the driver for the VSC 7385.
    
    Signed-off-by: Timur Tabi <timur@freescale.com>
    Signed-off-by: Paul Mackerras <paulus@samba.org>

commit 13a2eea1461f5d54cc5d58334fbde4bf4cc9cb23
Author: Nick Piggin <npiggin@suse.de>
Date:   Wed Oct 4 17:25:44 2006 +0200

    [POWERPC] Fix harmless typo
    
    Fix a typo. Noticed by the unlikely profiler.
    
    Signed-off-by: Nick Piggin <npiggin@suse.de>
    Signed-off-by: Paul Mackerras <paulus@samba.org>


Index: linux-2.6/arch/powerpc/boot/dts/mpc8349emitx.dts
===================================================================
--- /dev/null
+++ linux-2.6/arch/powerpc/boot/dts/mpc8349emitx.dts
@@ -0,0 +1,246 @@
+/*
+ * MPC8349E-mITX Device Tree Source
+ *
+ * Copyright 2006 Freescale Semiconductor Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU General Public License as published by the
+ * Free Software Foundation; either version 2 of the License, or (at your
+ * option) any later version.
+ */
+/ {
+	model = "MPC8349EMITX";
+	compatible = "MPC834xMITX";
+	#address-cells = <1>;
+	#size-cells = <1>;
+
+	cpus {
+		#cpus = <1>;
+		#address-cells = <1>;
+		#size-cells = <0>;
+
+		PowerPC,8349@0 {
+			device_type = "cpu";
+			reg = <0>;
+			d-cache-line-size = <20>;
+			i-cache-line-size = <20>;
+			d-cache-size = <8000>;
+			i-cache-size = <8000>;
+			timebase-frequency = <0>;	// from bootloader
+			bus-frequency = <0>;		// from bootloader
+			clock-frequency = <0>;		// from bootloader
+			32-bit;
+		};
+	};
+
+	memory {
+		device_type = "memory";
+		reg = <00000000 10000000>;
+	};
+
+	soc8349@e0000000 {
+		#address-cells = <1>;
+		#size-cells = <1>;
+		#interrupt-cells = <2>;
+		device_type = "soc";
+		ranges = <0 e0000000 00100000>;
+		reg = <e0000000 00000200>;
+		bus-frequency = <0>;                    // from bootloader
+
+		wdt@200 {
+			device_type = "watchdog";
+			compatible = "mpc83xx_wdt";
+			reg = <200 100>;
+		};
+
+		i2c@3000 {
+			device_type = "i2c";
+			compatible = "fsl-i2c";
+			reg = <3000 100>;
+			interrupts = <e 8>;
+			interrupt-parent = <700>;
+			dfsrr;
+		};
+
+		i2c@3100 {
+			device_type = "i2c";
+			compatible = "fsl-i2c";
+			reg = <3100 100>;
+			interrupts = <f 8>;
+			interrupt-parent = <700>;
+			dfsrr;
+		};
+
+		spi@7000 {
+			device_type = "spi";
+			compatible = "mpc83xx_spi";
+			reg = <7000 1000>;
+			interrupts = <10 8>;
+			interrupt-parent = <700>;
+			mode = <0>;
+		};
+
+		usb@22000 {
+			device_type = "usb";
+			compatible = "fsl-usb2-mph";
+			reg = <22000 1000>;
+			#address-cells = <1>;
+			#size-cells = <0>;
+			interrupt-parent = <700>;
+			interrupts = <27 2>;
+			phy_type = "ulpi";
+			port1;
+		};
+
+		usb@23000 {
+			device_type = "usb";
+			compatible = "fsl-usb2-dr";
+			reg = <23000 1000>;
+			#address-cells = <1>;
+			#size-cells = <0>;
+			interrupt-parent = <700>;
+			interrupts = <26 2>;
+			phy_type = "ulpi";
+		};
+
+		mdio@24520 {
+			device_type = "mdio";
+			compatible = "gianfar";
+			reg = <24520 20>;
+			#address-cells = <1>;
+			#size-cells = <0>;
+			linux,phandle = <24520>;
+
+			/* Vitesse 8201 */
+			ethernet-phy@1c {
+				linux,phandle = <245201c>;
+				interrupt-parent = <700>;
+				interrupts = <12 2>;
+				reg = <1c>;
+				device_type = "ethernet-phy";
+			};
+
+			/* Vitesse 7385 */
+			ethernet-phy@1f {
+				linux,phandle = <245201f>;
+				interrupt-parent = <700>;
+				interrupts = <12 2>;
+				reg = <1f>;
+				device_type = "ethernet-phy";
+			};
+		};
+
+		ethernet@24000 {
+			device_type = "network";
+			model = "TSEC";
+			compatible = "gianfar";
+			reg = <24000 1000>;
+			address = [ 00 00 00 00 00 00 ];
+			local-mac-address = [ 00 00 00 00 00 00 ];
+			interrupts = <20 8 21 8 22 8>;
+			interrupt-parent = <700>;
+			phy-handle = <245201c>;
+		};
+
+		ethernet@25000 {
+			#address-cells = <1>;
+			#size-cells = <0>;
+			device_type = "network";
+			model = "TSEC";
+			compatible = "gianfar";
+			reg = <25000 1000>;
+			address = [ 00 00 00 00 00 00 ];
+			local-mac-address = [ 00 00 00 00 00 00 ];
+			interrupts = <23 8 24 8 25 8>;
+			interrupt-parent = <700>;
+			phy-handle = <245201f>;
+		};
+
+		serial@4500 {
+			device_type = "serial";
+			compatible = "ns16550";
+			reg = <4500 100>;
+			clock-frequency = <0>;		// from bootloader
+			interrupts = <9 8>;
+			interrupt-parent = <700>;
+		};
+
+		serial@4600 {
+			device_type = "serial";
+			compatible = "ns16550";
+			reg = <4600 100>;
+			clock-frequency = <0>;		// from bootloader
+			interrupts = <a 8>;
+			interrupt-parent = <700>;
+		};
+
+		pci@8500 {
+			interrupt-map-mask = <f800 0 0 7>;
+			interrupt-map = <
+					/* IDSEL 0x10 - SATA */
+					8000 0 0 1 700 16 8 /* SATA_INTA */
+					>;
+			interrupt-parent = <700>;
+			interrupts = <42 8>;
+			bus-range = <0 0>;
+			ranges = <42000000 0 80000000 80000000 0 10000000
+				  02000000 0 90000000 90000000 0 10000000
+				  01000000 0 00000000 e2000000 0 01000000>;
+			clock-frequency = <3f940aa>;
+			#interrupt-cells = <1>;
+			#size-cells = <2>;
+			#address-cells = <3>;
+			reg = <8500 100>;
+			compatible = "83xx";
+			device_type = "pci";
+		};
+
+		pci@8600 {
+			interrupt-map-mask = <f800 0 0 7>;
+			interrupt-map = <
+					/* IDSEL 0x0E - MiniPCI Slot */
+					7000 0 0 1 700 15 8 /* PCI_INTA */
+
+					/* IDSEL 0x0F - PCI Slot */
+					7800 0 0 1 700 14 8 /* PCI_INTA */
+					7800 0 0 2 700 15 8 /* PCI_INTB */
+					 >;
+			interrupt-parent = <700>;
+			interrupts = <43 8>;
+			bus-range = <1 1>;
+			ranges = <42000000 0 a0000000 a0000000 0 10000000
+				  02000000 0 b0000000 b0000000 0 10000000
+				  01000000 0 00000000 e3000000 0 01000000>;
+			clock-frequency = <3f940aa>;
+			#interrupt-cells = <1>;
+			#size-cells = <2>;
+			#address-cells = <3>;
+			reg = <8600 100>;
+			compatible = "83xx";
+			device_type = "pci";
+		};
+
+		crypto@30000 {
+			device_type = "crypto";
+			model = "SEC2";
+			compatible = "talitos";
+			reg = <30000 10000>;
+			interrupts = <b 8>;
+			interrupt-parent = <700>;
+			num-channels = <4>;
+			channel-fifo-len = <18>;
+			exec-units-mask = <0000007e>;
+			descriptor-types-mask = <01010ebf>;
+		};
+
+		pic@700 {
+			linux,phandle = <700>;
+			interrupt-controller;
+			#address-cells = <0>;
+			#interrupt-cells = <2>;
+			reg = <700 100>;
+			built-in;
+			device_type = "ipic";
+		};
+	};
+};
Index: linux-2.6/arch/powerpc/boot/of.c
===================================================================
--- linux-2.6.orig/arch/powerpc/boot/of.c
+++ linux-2.6/arch/powerpc/boot/of.c
@@ -176,12 +176,9 @@ static void *claim(unsigned long virt, u
 static void *of_try_claim(u32 size)
 {
 	unsigned long addr = 0;
-	static u8 first_time = 1;
 
-	if (first_time) {
+	if (claim_base == 0)
 		claim_base = _ALIGN_UP((unsigned long)_end, ONE_MB);
-		first_time = 0;
-	}
 
 	for(; claim_base < RAM_END; claim_base += ONE_MB) {
 #ifdef DEBUG
Index: linux-2.6/arch/powerpc/configs/iseries_defconfig
===================================================================
--- linux-2.6.orig/arch/powerpc/configs/iseries_defconfig
+++ linux-2.6/arch/powerpc/configs/iseries_defconfig
@@ -1,7 +1,7 @@
 #
 # Automatically generated make config: don't edit
-# Linux kernel version: 2.6.18-rc6
-# Sun Sep 10 10:22:57 2006
+# Linux kernel version: 2.6.19-rc1
+# Fri Oct  6 13:25:04 2006
 #
 CONFIG_PPC64=y
 CONFIG_64BIT=y
@@ -22,6 +22,7 @@ CONFIG_ARCH_MAY_HAVE_PC_FDC=y
 CONFIG_PPC_OF=y
 # CONFIG_PPC_UDBG_16550 is not set
 # CONFIG_GENERIC_TBSYNC is not set
+CONFIG_AUDIT_ARCH=y
 # CONFIG_DEFAULT_UIMAGE is not set
 
 #
@@ -52,10 +53,11 @@ CONFIG_LOCALVERSION=""
 CONFIG_LOCALVERSION_AUTO=y
 CONFIG_SWAP=y
 CONFIG_SYSVIPC=y
+# CONFIG_IPC_NS is not set
 CONFIG_POSIX_MQUEUE=y
 # CONFIG_BSD_PROCESS_ACCT is not set
 # CONFIG_TASKSTATS is not set
-CONFIG_SYSCTL=y
+# CONFIG_UTS_NS is not set
 CONFIG_AUDIT=y
 CONFIG_AUDITSYSCALL=y
 CONFIG_IKCONFIG=y
@@ -64,7 +66,9 @@ CONFIG_IKCONFIG_PROC=y
 # CONFIG_RELAY is not set
 CONFIG_INITRAMFS_SOURCE=""
 CONFIG_CC_OPTIMIZE_FOR_SIZE=y
+CONFIG_SYSCTL=y
 # CONFIG_EMBEDDED is not set
+# CONFIG_SYSCTL_SYSCALL is not set
 CONFIG_KALLSYMS=y
 # CONFIG_KALLSYMS_ALL is not set
 # CONFIG_KALLSYMS_EXTRA_PASS is not set
@@ -73,12 +77,12 @@ CONFIG_PRINTK=y
 CONFIG_BUG=y
 CONFIG_ELF_CORE=y
 CONFIG_BASE_FULL=y
-CONFIG_RT_MUTEXES=y
 CONFIG_FUTEX=y
 CONFIG_EPOLL=y
 CONFIG_SHMEM=y
 CONFIG_SLAB=y
 CONFIG_VM_EVENT_COUNTERS=y
+CONFIG_RT_MUTEXES=y
 # CONFIG_TINY_SHMEM is not set
 CONFIG_BASE_SMALL=0
 # CONFIG_SLOB is not set
@@ -97,6 +101,7 @@ CONFIG_STOP_MACHINE=y
 #
 # Block layer
 #
+CONFIG_BLOCK=y
 # CONFIG_BLK_DEV_IO_TRACE is not set
 
 #
@@ -115,13 +120,18 @@ CONFIG_DEFAULT_IOSCHED="anticipatory"
 #
 # Platform support
 #
-# CONFIG_PPC_MULTIPLATFORM is not set
-CONFIG_PPC_ISERIES=y
+CONFIG_PPC_MULTIPLATFORM=y
 # CONFIG_EMBEDDED6xx is not set
 # CONFIG_APUS is not set
+# CONFIG_PPC_PSERIES is not set
+CONFIG_PPC_ISERIES=y
+# CONFIG_PPC_PMAC is not set
+# CONFIG_PPC_MAPLE is not set
+# CONFIG_PPC_PASEMI is not set
 # CONFIG_PPC_CELL is not set
 # CONFIG_PPC_CELL_NATIVE is not set
-# CONFIG_UDBG_RTAS_CONSOLE is not set
+# CONFIG_PPC_IBM_CELL_BLADE is not set
+# CONFIG_U3_DART is not set
 # CONFIG_PPC_RTAS is not set
 # CONFIG_MMIO_NVRAM is not set
 CONFIG_IBMVIO=y
@@ -147,12 +157,15 @@ CONFIG_BINFMT_ELF=y
 CONFIG_FORCE_MAX_ZONEORDER=13
 CONFIG_IOMMU_VMERGE=y
 CONFIG_ARCH_ENABLE_MEMORY_HOTPLUG=y
+# CONFIG_KEXEC is not set
+# CONFIG_CRASH_DUMP is not set
 CONFIG_IRQ_ALL_CPUS=y
 CONFIG_LPARCFG=y
 # CONFIG_NUMA is not set
 CONFIG_ARCH_SELECT_MEMORY_MODEL=y
 CONFIG_ARCH_FLATMEM_ENABLE=y
 CONFIG_ARCH_SPARSEMEM_ENABLE=y
+CONFIG_ARCH_POPULATES_NODE_MAP=y
 CONFIG_SELECT_MEMORY_MODEL=y
 CONFIG_FLATMEM_MANUAL=y
 # CONFIG_DISCONTIGMEM_MANUAL is not set
@@ -179,6 +192,7 @@ CONFIG_GENERIC_ISA_DMA=y
 CONFIG_PCI=y
 CONFIG_PCI_DOMAINS=y
 # CONFIG_PCIEPORTBUS is not set
+# CONFIG_PCI_MULTITHREAD_PROBE is not set
 # CONFIG_PCI_DEBUG is not set
 
 #
@@ -206,6 +220,7 @@ CONFIG_PACKET=y
 CONFIG_UNIX=y
 CONFIG_XFRM=y
 CONFIG_XFRM_USER=m
+CONFIG_XFRM_SUB_POLICY=y
 CONFIG_NET_KEY=m
 CONFIG_INET=y
 CONFIG_IP_MULTICAST=y
@@ -224,10 +239,12 @@ CONFIG_INET_XFRM_TUNNEL=m
 CONFIG_INET_TUNNEL=y
 CONFIG_INET_XFRM_MODE_TRANSPORT=y
 CONFIG_INET_XFRM_MODE_TUNNEL=y
+CONFIG_INET_XFRM_MODE_BEET=m
 CONFIG_INET_DIAG=y
 CONFIG_INET_TCP_DIAG=y
 # CONFIG_TCP_CONG_ADVANCED is not set
-CONFIG_TCP_CONG_BIC=y
+CONFIG_TCP_CONG_CUBIC=y
+CONFIG_DEFAULT_TCP_CONG="cubic"
 
 #
 # IP: Virtual Server Configuration
@@ -247,6 +264,7 @@ CONFIG_NETFILTER=y
 CONFIG_NETFILTER_XTABLES=m
 CONFIG_NETFILTER_XT_TARGET_CLASSIFY=m
 CONFIG_NETFILTER_XT_TARGET_CONNMARK=m
+CONFIG_NETFILTER_XT_TARGET_DSCP=m
 CONFIG_NETFILTER_XT_TARGET_MARK=m
 CONFIG_NETFILTER_XT_TARGET_NFQUEUE=m
 CONFIG_NETFILTER_XT_TARGET_NOTRACK=m
@@ -255,6 +273,7 @@ CONFIG_NETFILTER_XT_MATCH_CONNBYTES=m
 CONFIG_NETFILTER_XT_MATCH_CONNMARK=m
 CONFIG_NETFILTER_XT_MATCH_CONNTRACK=m
 # CONFIG_NETFILTER_XT_MATCH_DCCP is not set
+CONFIG_NETFILTER_XT_MATCH_DSCP=m
 # CONFIG_NETFILTER_XT_MATCH_ESP is not set
 CONFIG_NETFILTER_XT_MATCH_HELPER=m
 CONFIG_NETFILTER_XT_MATCH_LENGTH=m
@@ -294,7 +313,6 @@ CONFIG_IP_NF_MATCH_IPRANGE=m
 CONFIG_IP_NF_MATCH_TOS=m
 CONFIG_IP_NF_MATCH_RECENT=m
 CONFIG_IP_NF_MATCH_ECN=m
-CONFIG_IP_NF_MATCH_DSCP=m
 # CONFIG_IP_NF_MATCH_AH is not set
 CONFIG_IP_NF_MATCH_TTL=m
 CONFIG_IP_NF_MATCH_OWNER=m
@@ -319,7 +337,6 @@ CONFIG_IP_NF_NAT_AMANDA=m
 CONFIG_IP_NF_MANGLE=m
 CONFIG_IP_NF_TARGET_TOS=m
 CONFIG_IP_NF_TARGET_ECN=m
-CONFIG_IP_NF_TARGET_DSCP=m
 CONFIG_IP_NF_TARGET_TTL=m
 CONFIG_IP_NF_TARGET_CLUSTERIP=m
 CONFIG_IP_NF_RAW=m
@@ -351,7 +368,6 @@ CONFIG_LLC=y
 # CONFIG_ATALK is not set
 # CONFIG_X25 is not set
 # CONFIG_LAPB is not set
-# CONFIG_NET_DIVERT is not set
 # CONFIG_ECONET is not set
 # CONFIG_WAN_ROUTER is not set
 
@@ -433,6 +449,7 @@ CONFIG_BLK_DEV_INITRD=y
 #
 # CONFIG_RAID_ATTRS is not set
 CONFIG_SCSI=y
+CONFIG_SCSI_NETLINK=y
 CONFIG_SCSI_PROC_FS=y
 
 #
@@ -454,12 +471,14 @@ CONFIG_SCSI_CONSTANTS=y
 # CONFIG_SCSI_LOGGING is not set
 
 #
-# SCSI Transport Attributes
+# SCSI Transports
 #
 CONFIG_SCSI_SPI_ATTRS=y
 CONFIG_SCSI_FC_ATTRS=y
 # CONFIG_SCSI_ISCSI_ATTRS is not set
-# CONFIG_SCSI_SAS_ATTRS is not set
+CONFIG_SCSI_SAS_ATTRS=m
+CONFIG_SCSI_SAS_LIBSAS=m
+CONFIG_SCSI_SAS_LIBSAS_DEBUG=y
 
 #
 # SCSI low-level drivers
@@ -472,10 +491,11 @@ CONFIG_SCSI_FC_ATTRS=y
 # CONFIG_SCSI_AIC7XXX is not set
 # CONFIG_SCSI_AIC7XXX_OLD is not set
 # CONFIG_SCSI_AIC79XX is not set
+# CONFIG_SCSI_AIC94XX is not set
+# CONFIG_SCSI_ARCMSR is not set
 # CONFIG_MEGARAID_NEWGEN is not set
 # CONFIG_MEGARAID_LEGACY is not set
 # CONFIG_MEGARAID_SAS is not set
-# CONFIG_ATA is not set
 # CONFIG_SCSI_HPTIOP is not set
 # CONFIG_SCSI_BUSLOGIC is not set
 # CONFIG_SCSI_DMX3191D is not set
@@ -486,16 +506,22 @@ CONFIG_SCSI_FC_ATTRS=y
 CONFIG_SCSI_IBMVSCSI=m
 # CONFIG_SCSI_INITIO is not set
 # CONFIG_SCSI_INIA100 is not set
+# CONFIG_SCSI_STEX is not set
 # CONFIG_SCSI_SYM53C8XX_2 is not set
-# CONFIG_SCSI_IPR is not set
 # CONFIG_SCSI_QLOGIC_1280 is not set
 # CONFIG_SCSI_QLA_FC is not set
+# CONFIG_SCSI_QLA_ISCSI is not set
 # CONFIG_SCSI_LPFC is not set
 # CONFIG_SCSI_DC395x is not set
 # CONFIG_SCSI_DC390T is not set
 # CONFIG_SCSI_DEBUG is not set
 
 #
+# Serial ATA (prod) and Parallel ATA (experimental) drivers
+#
+# CONFIG_ATA is not set
+
+#
 # Multi-device support (RAID and LVM)
 #
 CONFIG_MD=y
@@ -508,6 +534,7 @@ CONFIG_MD_RAID10=m
 CONFIG_MD_MULTIPATH=m
 CONFIG_MD_FAULTY=m
 CONFIG_BLK_DEV_DM=y
+# CONFIG_DM_DEBUG is not set
 CONFIG_DM_CRYPT=m
 CONFIG_DM_SNAPSHOT=m
 CONFIG_DM_MIRROR=m
@@ -573,6 +600,7 @@ CONFIG_MII=y
 # CONFIG_HP100 is not set
 CONFIG_NET_PCI=y
 CONFIG_PCNET32=y
+CONFIG_PCNET32_NAPI=y
 # CONFIG_AMD8111_ETH is not set
 # CONFIG_ADAPTEC_STARFIRE is not set
 # CONFIG_B44 is not set
@@ -610,6 +638,7 @@ CONFIG_E1000=m
 # CONFIG_VIA_VELOCITY is not set
 # CONFIG_TIGON3 is not set
 # CONFIG_BNX2 is not set
+# CONFIG_QLA3XXX is not set
 
 #
 # Ethernet (10000 Mbit)
@@ -649,6 +678,7 @@ CONFIG_PPP_BSDCOMP=m
 # CONFIG_PPP_MPPE is not set
 CONFIG_PPPOE=m
 # CONFIG_SLIP is not set
+CONFIG_SLHC=m
 # CONFIG_NET_FC is not set
 # CONFIG_SHAPER is not set
 CONFIG_NETCONSOLE=y
@@ -671,6 +701,7 @@ CONFIG_NET_POLL_CONTROLLER=y
 # Input device support
 #
 CONFIG_INPUT=y
+# CONFIG_INPUT_FF_MEMLESS is not set
 
 #
 # Userland interfaces
@@ -774,12 +805,12 @@ CONFIG_MAX_RAW_DEVS=256
 #
 # Misc devices
 #
+# CONFIG_TIFM_CORE is not set
 
 #
 # Multimedia devices
 #
 # CONFIG_VIDEO_DEV is not set
-CONFIG_VIDEO_V4L2=y
 
 #
 # Digital Video Broadcasting Devices
@@ -893,6 +924,9 @@ CONFIG_XFS_FS=m
 CONFIG_XFS_SECURITY=y
 CONFIG_XFS_POSIX_ACL=y
 # CONFIG_XFS_RT is not set
+CONFIG_GFS2_FS=m
+CONFIG_GFS2_FS_LOCKING_NOLOCK=m
+CONFIG_GFS2_FS_LOCKING_DLM=m
 # CONFIG_OCFS2_FS is not set
 # CONFIG_MINIX_FS is not set
 # CONFIG_ROMFS_FS is not set
@@ -929,12 +963,14 @@ CONFIG_FAT_DEFAULT_IOCHARSET="iso8859-1"
 #
 CONFIG_PROC_FS=y
 CONFIG_PROC_KCORE=y
+CONFIG_PROC_SYSCTL=y
 CONFIG_SYSFS=y
 CONFIG_TMPFS=y
+CONFIG_TMPFS_POSIX_ACL=y
 # CONFIG_HUGETLBFS is not set
 # CONFIG_HUGETLB_PAGE is not set
 CONFIG_RAMFS=y
-# CONFIG_CONFIGFS_FS is not set
+CONFIG_CONFIGFS_FS=m
 
 #
 # Miscellaneous filesystems
@@ -988,6 +1024,7 @@ CONFIG_CIFS_POSIX=y
 # CONFIG_CODA_FS is not set
 # CONFIG_AFS_FS is not set
 # CONFIG_9P_FS is not set
+CONFIG_GENERIC_ACL=y
 
 #
 # Partition Types
@@ -1040,6 +1077,12 @@ CONFIG_NLS_ISO8859_1=y
 # CONFIG_NLS_UTF8 is not set
 
 #
+# Distributed Lock Manager
+#
+CONFIG_DLM=m
+# CONFIG_DLM_DEBUG is not set
+
+#
 # iSeries device drivers
 #
 CONFIG_VIOCONS=y
@@ -1073,6 +1116,7 @@ CONFIG_PLIST=y
 # Kernel hacking
 #
 # CONFIG_PRINTK_TIME is not set
+CONFIG_ENABLE_MUST_CHECK=y
 CONFIG_MAGIC_SYSRQ=y
 # CONFIG_UNUSED_SYMBOLS is not set
 CONFIG_DEBUG_KERNEL=y
@@ -1091,6 +1135,7 @@ CONFIG_DETECT_SOFTLOCKUP=y
 # CONFIG_DEBUG_INFO is not set
 CONFIG_DEBUG_FS=y
 # CONFIG_DEBUG_VM is not set
+# CONFIG_DEBUG_LIST is not set
 # CONFIG_FORCED_INLINING is not set
 # CONFIG_RCU_TORTURE_TEST is not set
 CONFIG_DEBUG_STACKOVERFLOW=y
@@ -1109,6 +1154,10 @@ CONFIG_IRQSTACKS=y
 # Cryptographic options
 #
 CONFIG_CRYPTO=y
+CONFIG_CRYPTO_ALGAPI=y
+CONFIG_CRYPTO_BLKCIPHER=m
+CONFIG_CRYPTO_HASH=y
+CONFIG_CRYPTO_MANAGER=m
 CONFIG_CRYPTO_HMAC=y
 CONFIG_CRYPTO_NULL=m
 CONFIG_CRYPTO_MD4=m
@@ -1118,9 +1167,12 @@ CONFIG_CRYPTO_SHA256=m
 CONFIG_CRYPTO_SHA512=m
 CONFIG_CRYPTO_WP512=m
 CONFIG_CRYPTO_TGR192=m
+CONFIG_CRYPTO_ECB=m
+CONFIG_CRYPTO_CBC=m
 CONFIG_CRYPTO_DES=y
 CONFIG_CRYPTO_BLOWFISH=m
 CONFIG_CRYPTO_TWOFISH=m
+CONFIG_CRYPTO_TWOFISH_COMMON=m
 CONFIG_CRYPTO_SERPENT=m
 CONFIG_CRYPTO_AES=m
 CONFIG_CRYPTO_CAST5=m
Index: linux-2.6/arch/powerpc/configs/pseries_defconfig
===================================================================
--- linux-2.6.orig/arch/powerpc/configs/pseries_defconfig
+++ linux-2.6/arch/powerpc/configs/pseries_defconfig
@@ -506,7 +506,7 @@ CONFIG_SCSI_SAS_ATTRS=m
 # CONFIG_MEGARAID_NEWGEN is not set
 # CONFIG_MEGARAID_LEGACY is not set
 # CONFIG_MEGARAID_SAS is not set
-# CONFIG_ATA is not set
+CONFIG_ATA=y
 # CONFIG_SCSI_HPTIOP is not set
 # CONFIG_SCSI_BUSLOGIC is not set
 # CONFIG_SCSI_DMX3191D is not set
Index: linux-2.6/arch/powerpc/kernel/iommu.c
===================================================================
--- linux-2.6.orig/arch/powerpc/kernel/iommu.c
+++ linux-2.6/arch/powerpc/kernel/iommu.c
@@ -75,7 +75,7 @@ static unsigned long iommu_range_alloc(s
 	/* This allocator was derived from x86_64's bit string search */
 
 	/* Sanity check */
-	if (unlikely(npages) == 0) {
+	if (unlikely(npages == 0)) {
 		if (printk_ratelimit())
 			WARN_ON(1);
 		return DMA_ERROR_CODE;
Index: linux-2.6/arch/powerpc/kernel/prom.c
===================================================================
--- linux-2.6.orig/arch/powerpc/kernel/prom.c
+++ linux-2.6/arch/powerpc/kernel/prom.c
@@ -724,7 +724,7 @@ static int __init early_init_dt_scan_cho
 		strlcpy(cmd_line, p, min((int)l, COMMAND_LINE_SIZE));
 
 #ifdef CONFIG_CMDLINE
-	if (l == 0 || (l == 1 && (*p) == 0))
+	if (p == NULL || l == 0 || (l == 1 && (*p) == 0))
 		strlcpy(cmd_line, CONFIG_CMDLINE, COMMAND_LINE_SIZE);
 #endif /* CONFIG_CMDLINE */
 
Index: linux-2.6/arch/powerpc/kernel/prom_parse.c
===================================================================
--- linux-2.6.orig/arch/powerpc/kernel/prom_parse.c
+++ linux-2.6/arch/powerpc/kernel/prom_parse.c
@@ -914,6 +914,17 @@ int of_irq_map_pci(struct pci_dev *pdev,
 	u8 pin;
 	int rc;
 
+	/* We need to first check if the PCI device has a PCI interrupt at all
+	 * since we have cases where the device-node might expose non-PCI
+	 * interrupts, but the device has no PCI interrupt to it
+	 */
+	rc = pci_read_config_byte(pdev, PCI_INTERRUPT_PIN, &pin);
+	if (rc != 0)
+		return rc;
+	/* No pin, exit */
+	if (pin == 0)
+		return -ENODEV;
+
 	/* Check if we have a device node, if yes, fallback to standard OF
 	 * parsing
 	 */
@@ -925,12 +936,6 @@ int of_irq_map_pci(struct pci_dev *pdev,
 	 * interrupt spec.  we assume #interrupt-cells is 1, which is standard
 	 * for PCI. If you do different, then don't use that routine.
 	 */
-	rc = pci_read_config_byte(pdev, PCI_INTERRUPT_PIN, &pin);
-	if (rc != 0)
-		return rc;
-	/* No pin, exit */
-	if (pin == 0)
-		return -ENODEV;
 
 	/* Now we walk up the PCI tree */
 	lspec = pin;
Index: linux-2.6/arch/powerpc/platforms/iseries/smp.h
===================================================================
--- /dev/null
+++ linux-2.6/arch/powerpc/platforms/iseries/smp.h
@@ -0,0 +1,6 @@
+#ifndef _PLATFORMS_ISERIES_SMP_H
+#define _PLATFORMS_ISERIES_SMP_H
+
+extern void iSeries_smp_message_recv(void);
+
+#endif	/* _PLATFORMS_ISERIES_SMP_H */
Index: linux-2.6/arch/powerpc/platforms/pseries/iommu.c
===================================================================
--- linux-2.6.orig/arch/powerpc/platforms/pseries/iommu.c
+++ linux-2.6/arch/powerpc/platforms/pseries/iommu.c
@@ -267,7 +267,8 @@ static void iommu_table_setparms(struct 
 				 struct iommu_table *tbl)
 {
 	struct device_node *node;
-	const unsigned long *basep, *sizep;
+	const unsigned long *basep;
+	const u32 *sizep;
 
 	node = (struct device_node *)phb->arch_data;
 
Index: linux-2.6/arch/powerpc/sysdev/fsl_soc.c
===================================================================
--- linux-2.6.orig/arch/powerpc/sysdev/fsl_soc.c
+++ linux-2.6/arch/powerpc/sysdev/fsl_soc.c
@@ -567,7 +567,7 @@ static int __init fs_enet_of_init(void)
 		struct resource r[4];
 		struct device_node *phy, *mdio;
 		struct fs_platform_info fs_enet_data;
-		const unsigned int *id, *phy_addr, phy_irq;
+		const unsigned int *id, *phy_addr, *phy_irq;
 		const void *mac_addr;
 		const phandle *ph;
 		const char *model;
@@ -641,7 +641,7 @@ static int __init fs_enet_of_init(void)
 
 		if (strstr(model, "FCC")) {
 			int fcc_index = *id - 1;
-			unsigned char* mdio_bb_prop;
+			const unsigned char *mdio_bb_prop;
 
 			fs_enet_data.dpram_offset = (u32)cpm_dpram_addr(0);
 			fs_enet_data.rx_ring = 32;
@@ -708,8 +708,9 @@ static int __init fs_enet_of_init(void)
 			ret = platform_device_add_data(fs_enet_dev, &fs_enet_data,
 						     sizeof(struct
 							    fs_platform_info));
-		if (ret)
-			goto unreg;
+			if (ret)
+				goto unreg;
+		}
 	}
 	return 0;
 
Index: linux-2.6/drivers/char/viocons.c
===================================================================
--- linux-2.6.orig/drivers/char/viocons.c
+++ linux-2.6/drivers/char/viocons.c
@@ -947,7 +947,7 @@ static void vioHandleData(struct HvLpEve
 				 */
 				continue;
 			} else if (vio_sysrq_pressed) {
-				handle_sysrq(cevent->data[index], NULL, tty);
+				handle_sysrq(cevent->data[index], tty);
 				vio_sysrq_pressed = 0;
 				/*
 				 * continue because we don't want to add
Index: linux-2.6/include/asm-powerpc/reg.h
===================================================================
--- linux-2.6.orig/include/asm-powerpc/reg.h
+++ linux-2.6/include/asm-powerpc/reg.h
@@ -503,7 +503,7 @@
 
 /*
  * An mtfsf instruction with the L bit set. On CPUs that support this a
- * full 64bits of FPSCR is restored and on other CPUs it is ignored.
+ * full 64bits of FPSCR is restored and on other CPUs the L bit is ignored.
  *
  * Until binutils gets the new form of mtfsf, hardwire the instruction.
  */
