# ps3linux - an independent repository for PS3 Linux & Cell development

---

![Alt text](https://aethyx.eu/wp-content/uploads/linuxonps3.jpg?raw=true "PS3 Linux FTW!")

---

This repository started with a backup of the ps3linux directory left from `gitorious.ps3dev.net`. Unfortunately Gitorious is no more. So what we try here is to preserve what was left before the site shut down.

The preservations can be found in the following folders:

* Kernel patches and configs - applies for the official Linux kernel until 3.16
* Misc. - drivers and source codes, sometimes leading to those patches

But there is more, as we are constantly adding our own supplements.

PS3 Linux developers, and especially those interested in Cell B.E. CPU development, will find the following inside this repository:

* Debian 8.x (Jessie) packages of the Cell SDK 3.1 as well as
* all what was left on the YDL ISOs regarding SPU and PPU development, now as Debian 8.x packages
* all what was once left by the Barcelona Supercomputing Center regarding official Cell development, also as Debian 8.x packages
* Cell SDK documentations - here we collect all documentation we can find around Cell CPU development
* Package manager source lists - to get packages via online update managers for YDL 6.x & Debian 8.x
* Precompiled Debian 8.x (Jessie) kernels - find here the official "Red Ribbon" PS3 Linux kernel as well as those compiled by us
* a new folder inside "Kernel patches and configs" summarising our experiences compiling kernel 4.18

---

## Abstract

Browse the repository and watch carefully what you're looking for. Sometimes there exist README.md files, read them thoroughly.

Unfortunately patches for the official kernel tree seem to stop with version 3.16. But these shouldn't be needed anymore.

There exists a PS3 kernel tree still today: [https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git](https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git "https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git") maintained by Geoff Levand.
Be aware that due to a ABI bug the most recent kernel is 3.15 you may use on your own PS3 Linux machine (status: December 2017). Be also aware that this tree is only guaranteed to work with OFW <= 3.15.

Feel free to contribute!

---

## Motivation

We at AETHYX MEDIAE are still trying to program internally for the Cell Broadband Engine. It's a side project.
Thus, from time to time we'll bring back stuff online we used in the past. Also docs and developer files are planned.
Thirdly, plans for publishing our stuff in the near future are under (heavy) construction.

---

## Contact

Feel free to drop us a line anytime: info [at] aethyx [dot] eu.

Thanks and have a good time!

---

## Donations

Setting up and creating this repository was a hell of work and consumed a lot of time and nerves, so donations are warmly welcome.

Please use the following addresses to send us donations:

BTC: 1Df8eapdFuxPxteZLfXnwvyVuo574psxKM

ETH: 0x0D2Ab63dfe70a7fA12f9d66eCfEA9dDc8F5173A8

XEM: NBZPMU-XES6ST-ITEBR3-IHAPTR-APGI3Y-RAAMHV-VZFJ
